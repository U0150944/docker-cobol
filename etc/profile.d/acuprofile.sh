#!/bin/ksh
#-----------------------------------------------------------------------
# Configuracion del Entorno COR
#
# Autor: Horacio Ardissino
#-----------------------------------------------------------------------

#--------------------------------------------------------------------
# Variables Generales
#--------------------------------------------------------------------
PATH=$PATH:/usr/sartas:/noperiodicos/usr/sartas
export PATH
EQUIPO="system"
export EQUIPO
LOGNAME="u0150944"
export LOGNAME
PLATAFORMA=`uname -s`
export PLATAFORMA

#--------------------------------------------------------------------
# Variables de entorno de COBOL
#--------------------------------------------------------------------
COBOL_HOME=/usr/acucobol
export COBOL_HOME
PATH=$PATH:$COBOL_HOME:$COBOL_HOME/bin:$COBOL_HOME/tools:/$COBOL_HOME/etc:.
LD_LIBRARY_PATH_64=$LD_LIBRARY_PATH_64:$COBOL_HOME/lib:/usr/local/lib:
export LD_LIBRARY_PATH_64
SHARED_LIBRARY_LIST=$SHARED_LIBRARY_LIST:/usr/local/lib/conectividad.so:/usr/local/lib/encriptacion.so:
export SHARED_LIBRARY_LIST
LD_LIBRARY_PATH=$COBOL_HOME/bin:$COBOL_HOME/lib
export LD_LIBRARY_PATH
APPLY_FILE_PATH=1
export APPLY_FILE_PATH
FILE_PREFIX=/$EQUIPO:
export FILE_PREFIX
SCRIPT_PREFIX=/$EQUIPO
export SCRIPT_PREFIX
APPLY_CODE_PATH=1
export APPLY_CODE_PATH
CODE_PREFIX=/:/noperiodicos:.
export CODE_PREFIX
HTML_TEMPLATE_PREFIX=/
export HTML_TEMPLATE_PREFIX
A_TERM=ansi
export A_TERM
export PATH
#
#--------------------------------------------------------------------
# Variables de entorno de MQM
#--------------------------------------------------------------------
MQM_HOME=/opt/mqm
export MQM_HOME
QMANAGER=TNQMDESA
export QMANAGER
MQSERVER='TN.ADMIN.SVRCONN/TCP/192.168.221.58(1411)'
export MQSERVER
PATH=$PATH:$MQM_HOME/bin:$MQM_HOME/samp/bin
SHARED_LIBRARY_LIST=$SHARED_LIBRARY_LIST:/opt/mqm/lib64/libmqm.so
export SHARED_LIBRARY_LIST
#
#--------------------------------------------------------------------
# Variables de entorno de ORACLE
#--------------------------------------------------------------------
ORACLE_HOME=/$EQUIPO/home/oracle11g/app
export ORACLE_HOME
TWO_TASK=TNPRODESA01
export TWO_TASK
ORACLE_SID=TNPRODESA01
export ORACLE_SID
TNS_ADMIN=$ORACLE_HOME/network/admin
export TNS_ADMIN
LD_LIBRARY_PATH_64=$LD_LIBRARY_PATH_64:$ORACLE_HOME/lib
export LD_LIBRARY_PATH_64
PATH=$PATH:$ORACLE_HOME/bin
export PATH
#
