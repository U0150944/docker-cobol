#!/bin/bash
#-----------------------------------------------------------------------
# Administrador de Servicio TCP
#
# Autor: Horacio Ardissino
#-----------------------------------------------------------------------

PLATAFORMA=`uname -s`

#########################################################################
# Evaluacion de la plataforma y seteo de path de libreria ACE.
#########################################################################

case ${PLATAFORMA} in
'AIX')
    ;;
'SunOS')
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/ACE
    export NLS_LANG=SPANISH_SPAIN.WE8ISO8859P1
    ;;
'Linux')
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/ACE:/usr/local/lib:
    export NLS_LANG=SPANISH_SPAIN.WE8ISO8859P1    
    ;;
*)
    echo "La plataforma ${PLATAFORMA} no esta soportada para la ejecucion"
    exit 1
    ;;
esac

export PID=0
export INTERFAZ_MENSAJE=5

export SRVTCP_BIN=/disk3/soporte
export SRVTCP_LOG_PATH=/home/srvtcp/log
export SRVTCP_CFG_PATH=/usr/sartas

Uso() {
    echo "Uso:"
    echo "   srvtcp.sh {argumentos}"
    echo "     Argumentos:"
    echo "            list"
    echo "            [id] start"
    echo "            [id] stop"
    echo "            [id] suspend"
    echo "            [id] restart"
    echo "            [id] pid"
    echo "            [id] listen"
    echo "            [id] services"
    echo "            [id] dump"
    echo "            [id] debug   [nivel de log 0,10,20,30,40,50,60,100]"
    echo "            [id] status  [servicio|all]"
    echo "            [id] clear   [servicio|all]"
    echo "            [id] enable  [servicio|all]"
    echo "            [id] disable [servicio|all]"
    echo "            [id] load    [servicio]     [instancias]"
    echo "            [id] unload  [servicio|all] [instancias]"
    exit 1
}

ProcesoID() {
    PID=`ps -def |grep "$SRVTCP_BIN/srvtcp -i $1 -o start" |grep -v grep| gawk '{print $2;}'`
}

ListaProcesos() {
    echo "<Instancias del administrador de servicios:"
    ps -e -o "pid,args" |grep "$SRVTCP_BIN/srvtcp -i" | grep "\-o start" |grep -v grep| gawk '{print "Pid:"$1 " ID:"$4;}'
    echo ">"
}

case $1 in
"list")
    ListaProcesos
    exit 0
    ;;
"")
    Uso
    exit 1
    ;;
esac

case $2 in
"list")
    ListaProcesos
    exit 0
    ;;
"start")
    cd /$EQUIPO/usr/cobol/tmp
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "ERROR:El administrador de servicios [$1] esta iniciado, PID:[$PID]"
       exit 1
    fi
    nohup $SRVTCP_BIN/srvtcp -i $1 -o $2  2>&1 &
    sleep 5
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "Se inicio el administrador de servicios ID:[$1] PID:[$PID]"
       echo "Directorio de los Procesos:" $CODE_PREFIX
       exit 0
    else
       echo "ERROR:No se inicio el administrador de servicios ID:[$1]"
       exit 1
    fi
    ;;

"stop")
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "Deteniendo el Administrador de Servicios..."
       kill -usr1 $PID
       echo "Se detuvo el Administrador de Servicios ID:[$1] PID:[$PID]"
       exit 0
    else
       echo "ERROR:No esta ejecutandose el Administrador de Servicios ID:[$1]"
       exit 1
    fi
    ;;

"suspend")
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "Desactivando Servicios..."
       $SRVTCP_BIN/srvtcp -i $1 -o "disable|all"
       echo "Descargando Instancias..."
       $SRVTCP_BIN/srvtcp -i $1 -o "unload|all"
       exit 0
    else
       echo "ERROR:No esta ejecutandose el Administrador de Servicios ID:[$1]"
       exit 1
    fi
    ;;

"restart")
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "Activando Servicios..."
       $SRVTCP_BIN/srvtcp -i $1 -o "enable|all"
       exit 0
    else
       echo "ERROR:No esta ejecutandose el Administrador de Servicios ID:[$1]"
       exit 1
    fi
    ;;

"pid")
    ProcesoID $1
    if [ -n "$PID" ]
    then
       echo "ID:"$1 " PID:"$PID
       exit 0
    else
       echo "ERROR:No esta ejecutandose el Administrador de Servicios ID:[$1]"
       exit 1
    fi
    ;;

"listen" | "services" | "dump" | "debug" |"status" | "clear" | "enable" | "disable" | "load" | "unload")
    ProcesoID $1
    if [ -n "$PID" ]
    then
       $SRVTCP_BIN/srvtcp -i $1 -o "$2|$3|$4"
       exit 0
    else
       echo "ERROR:No esta ejecutandose el Administrador de Servicios ID:[$1]"
       exit 1
    fi
    ;;

*)
    Uso
    exit 1
    ;;
esac
exit 0

