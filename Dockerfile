# Based on Image
FROM ubuntu:18.04

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Update OS
RUN apt-get update && apt-get upgrade -y

# Install packages
RUN apt-get install -y alien vim openssh-client sudo gawk expect net-tools ksh

# Genero directorio para instalar librerias
RUN mkdir /usr/acucobol && mkdir /usr/sartas && mkdir -p /usr/local/lib/ACE && mkdir -p /usr/lib64

# Copia archivos necesarios al container..
ADD ./etc /etc/
ADD ./usr/sartas/ /usr/sartas/
ADD ./usr_local_lib /usr/local/lib/
ADD ./runtime/7.5.0.9-WS-MQC-LinuxX64.tar.gz /tmp/
ADD ./runtime/acucob922.tar /usr/acucobol/ 
ADD ./runtime/install_cobol_9 /tmp/ 
ADD ./runtime/install_mq_7 /tmp/

# Acepto la licencia de MQ
RUN chmod o+x /tmp/* && /tmp/install_mq_7 

# Instalo MQ
RUN \
rpm -ivh --force-debian --ignorearch /tmp/MQSeriesRuntime-7.5.0-9.x86_64.rpm \
&& rpm -ivh --force-debian --ignorearch /tmp/MQSeriesClient-7.5.0-9.x86_64.rpm \
&& rpm -ivh --force-debian --ignorearch /tmp/MQSeriesSamples-7.5.0-9.x86_64.rpm 

# Instancio instalacion MQ
RUN /opt/mqm/bin/setmqinst -i -p /opt/mqm

# Licencio Cobol 9.2
RUN /tmp/install_cobol_9 LSCSAU4L2WN6ULYY N8FBCW4L2S8H

# User for mq queue & socket connections
RUN addgroup hacmp \
&& addgroup srvtcp \
&& useradd -rm -d /home/srvtcp -s /bin/ksh -c "Administrador de servicios TCP/IP Cobol" -g srvtcp srvtcp \
&& usermod -g mqm -G hacmp srvtcp

# useradd -rm -d /home/mqm -s /bin/ksh -c "Ibm Mq Client" -g srvtcp mqm \

# copio los cfg y bin a los directorios
ADD ./skeleton/srvtcp_home.tar /home/srvtcp
#ADD ./srvtcp_home /home/srvtcp

# Asignacion de permisos..
RUN chmod +x /usr/sartas/* && chmod +x /etc/profile.d/acuprofile.sh && chown -R mqm:mqm /opt/mqm/* && chown -R srvtcp:hacmp /home/srvtcp/*

# Borro archivos innecesarios
#RUN rm -rf /tmp/*

# Me cambio al usuario que genere
USER srvtcp

#me cambio al directorio home del usuario
WORKDIR /home/srvtcp/bin

# ENTRYPOINT
#ENTRYPOINT ["/usr/sartas/start.sh"]
CMD ["sleep","infinity"]


