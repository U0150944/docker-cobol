#!/bin/bash
#set -x
##================================================================================#
##===============================FUNCIONES SECUNDARIAS============================#

EstadosPuertos()
{
    ESTADO_NET=`netstat -an | grep LISTEN | nawk '{print $4}' | sed 's!*.!!'`
    PUERTOS=`ls $SRVTCP_BIN_RUTA | grep "srvtcp-$SERVIDOR-"\....".sh$"`
    #printf "# Puerto   EstadoEscucha   Nombre                 ACTIVO | ALERTA |  ERROR\n"
    #printf "_____________________________________________________________________________\n"
    for i in `echo "$PUERTOS"`
    do 
        (
        PUERTO=`echo "$i" | nawk -F'-' '{print $NF}' | sed 's/.sh//'`
        ESCUCHA_PUERTO=`echo "$ESTADO_NET" | grep -E "^$PUERTO$"`
        if [ -z "$ESCUCHA_PUERTO" ]; then
            EP_ESTADO="INACTIVO"
        #    printf "  %-8d %-15s %-22s %-6d | %-6d | %-6d\n" $PUERTO $EP_ESTADO srvtcp-$SERVIDOR-$PUERTO 0 0 0
			SERVICIOS_ESTADO=`$SRVTCP_BIN_RUTA/$i status all | grep "^\_"`
            if [ ! -z "$SERVICIOS_ESTADO" ]; then 
                SERVICIO_ACTIVO=`echo "$SERVICIOS_ESTADO" | grep -i ACTIVO | wc -l | sed 's/ *//'`
                SERVICIO_ALERTA=`echo "$SERVICIOS_ESTADO" | grep -i ALERTA | wc -l | sed 's/ *//'`
                if [ $SERVICIO_ALERTA -ne 0 ]; then
                    printf "1" > $SRVTCP_LOG_RUTA/alerta.tmp
                fi
                SERVICIO_ERROR=`echo "$SERVICIOS_ESTADO" | grep -i ERROR | wc -l | sed 's/ *//'`
                if [ $SERVICIO_ERROR -ne 0 ]; then
                    printf "1" > $SRVTCP_LOG_RUTA/error.tmp
                fi
            fi
            #printf "  %-8d %-15s %-22s %-6d | %-6d | %-6d\n" $PUERTO $EP_ESTADO srvtcp-$SERVIDOR-$PUERTO $SERVICIO_ACTIVO $SERVICIO_ALERTA $SERVICIO_ERROR     
        else
            EP_ESTADO="ACTIVO"
            SERVICIOS_ESTADO=`$SRVTCP_BIN_RUTA/$i status all | grep "^\_"`
            if [ ! -z "$SERVICIOS_ESTADO" ]; then 
                SERVICIO_ACTIVO=`echo "$SERVICIOS_ESTADO" | grep -i ACTIVO | wc -l | sed 's/ *//'`
                SERVICIO_ALERTA=`echo "$SERVICIOS_ESTADO" | grep -i ALERTA | wc -l | sed 's/ *//'`
                if [ $SERVICIO_ALERTA -ne 0 ]; then
                    printf "1" > $SRVTCP_LOG_RUTA/alerta.tmp
                fi
                SERVICIO_ERROR=`echo "$SERVICIOS_ESTADO" | grep -i ERROR | wc -l | sed 's/ *//'`
                if [ $SERVICIO_ERROR -ne 0 ]; then
                    printf "1" > $SRVTCP_LOG_RUTA/error.tmp
                fi
            fi
            #printf "  %-8d %-15s %-22s %-6d | %-6d | %-6d\n" $PUERTO $EP_ESTADO srvtcp-$SERVIDOR-$PUERTO $SERVICIO_ACTIVO $SERVICIO_ALERTA $SERVICIO_ERROR     
        fi
        ) &
	done
	wait
}

ProcesoID() 
{
    PID=`ps -def | grep "$SRVTCP_PROG -i $1 -o start" | grep -v grep | nawk '{print $2;}'`
}

##================================================================================#
##============================= VARIABLES ========================================#
##================================================================================#

FECHA_LOG=$(date +%Y%m%d)
HORA=$(date +%H:%M)
USUARIO=`whoami`
SERVIDOR=`uname -n`
SO=`uname -s`
declare -i ALERTA=0 ERROR=0

##================================================================================#
##============================= FUNCION PRINCIPAL ================================#
##================================================================================#

if [ "$SO" = "AIX" ]
then
	. /etc/acuprofile.sh
	SRVTCP_BIN_RUTA="/$SERVIDOR/home/srvtcp/bin"
	SRVTCP_CFG_RUTA="/$SERVIDOR/home/srvtcp/cfg"
	SRVTCP_LOG_RUTA="/$SERVIDOR/home/srvtcp/log"
	SRVTCP_PROG="/$SERVIDOR/home/srvtcp/bin/srvtcp.sh"
	/$SERVIDOR/home/srvtcp/bin/srvtcp-system-5000.sh start
else
	. /etc/profile.d/acuprofile.sh
	if [ "$?" = "0" ]
	then
	 	printf "Variables de entorno seteadas correctamente\n"
	else
		printf "No pudo setearse las variable de entorno\n"
		exit 1
	fi
	SRVTCP_BIN_RUTA="/usr/sartas"
        SRVTCP_CFG_RUTA="/usr/sartas"
        SRVTCP_LOG_RUTA="/home/srvtcp/log"
	SRVTCP_PROG="/usr/sartas/srvtcp.sh"
	$SRVTCP_BIN_RUTA/srvtcp-online-4100.sh start
fi 

while true
do
	case $# in
		0) EstadosPuertos
		   PROC=`/$SRVTCP_BIN_RUTA/srvtcp-online-4100.sh pid`
		   ERROR=$(head -1 $SRVTCP_LOG_RUTA/error.tmp 2>/dev/null)
		   ALERTA=$(head -1 $SRVTCP_LOG_RUTA/alerta.tmp 2>/dev/null)
		   rm $SRVTCP_LOG_RUTA/error.tmp 2>/dev/null $SRVTCP_LOG_RUTA/alerta.tmp 2>/dev/null
		   if  [ $ERROR -eq 1 -a $ALERTA -eq 1 ]; then
				printf "Al menos 1 servicio con erro o alerta ${HORA}\nProceso:$PROC"
		   elif [ $ERROR -eq 1 ]; then
				printf "Al menos 1 servicio con error ${HORA}\nProceso:$PROC\n"

		   elif [ $ALERTA -eq 1 ]; then
				printf "Al menos 1 servicio con alertas ${HORA}\nProceso:$PROC\n"
		   else
				printf "Servicios funcionando correctamente ${HORA}\nProceso:$PROC\n"
		   fi
		   ;;
		1) 
		   ;;
		*) continue
		   ;;
	esac
	HORA=`date +%H:%M`;sleep 300
done


